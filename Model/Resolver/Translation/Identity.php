<?php
/**
 * Mobicommerce
 * Copyright (C) 2021 Mobicommerce <info@mobicommerce.net>
 *
 * @category Mobicommerce
 * @package Mobicommerce_TranslationGraphQl
 * @copyright Copyright (c) 2021 Mobicommerce (http://www.mobicommerce.net/)
 * @license http://opensource.org/licenses/gpl-3.0.html GNU General Public License,version 3 (GPL-3.0)
 * @author Mobicommerce <info@mobicommerce.net>
 */

namespace Mobicommerce\TranslationGraphQl\Model\Resolver\Translation;

use Magento\Framework\GraphQl\Query\Resolver\IdentityInterface;
use Mobicommerce\Mobiapp\Model\Translation as TranslationModel;

/**
 * Identity for resolved PageBuilder
 */
class Identity implements IdentityInterface
{
    /**
     * Get PageBuilder ID from resolved data
     *
     * @param array $resolvedData
     * @return string[]
     */
    public function getIdentities(array $resolvedData): array
    {
        $ids = [TranslationModel::CACHE_TAG];
        return $ids;
    }
}
